import 'package:edok/ui/cities/city.dart';
import 'package:flutter/material.dart';
import './ui/main/home.dart';
import 'login_page.dart';




void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      debugShowCheckedModeBanner: false,
      home: LoginPage(),
    );
  }
}
