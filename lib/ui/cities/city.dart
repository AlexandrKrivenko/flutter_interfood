import 'package:edok/ui/main/home.dart';
import 'package:edok/ui/places/places.dart';
import 'package:flutter/material.dart';
import 'dart:async';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:flutter/services.dart';

class CitiesName extends StatefulWidget {
  const CitiesName({Key key, @required this.cities}) : super(key: key);
  final Map cities;
  @override
  _CitiesNameState createState() => _CitiesNameState();
}

class _CitiesNameState extends State<CitiesName> {
  Map cities;
  List CitiesList;
  Future getData() async {
    http.Response response = await http
        .get("https://edok.kz/api/rest/cities?id_country=1", headers: {
      "x-rest-username": "mp@admplaces",
      "x-rest-password": "mp@7uf98HKHf"
    });
    cities = json.decode(response.body);
    setState(() {
      CitiesList = cities["cities"];
    });
  }

  @override
  void initState() {
    super.initState();
    getData();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
            title: Text(
              'Выберите город',
              style: TextStyle(
                color: Colors.black,
                fontSize: 16,
                fontWeight: FontWeight.w700,
              ),
              textAlign: TextAlign.center,
            ),
            actions: <Widget>[
              Container(
                child: IconButton(icon: Icon(Icons.search), onPressed: () {}),
                padding: EdgeInsets.all(10),
              ),
            ],
            centerTitle: true,
            iconTheme: IconThemeData(color: Colors.black),
            backgroundColor: Colors.white,
            brightness: Brightness.dark,
            elevation: 5),
        body: ListView.builder(
            itemCount: CitiesList == null ? 0 : CitiesList.length,
            itemBuilder: (BuildContext context, int index) {
              return CitiesIdList(cityId: CitiesList[index],);
            }));
  }
}

class CitiesIdList extends StatefulWidget {
	const CitiesIdList({
		Key key,
		@required this.cityId
}) : super(key: key);
	final Map cityId;
  @override
  _CitiesIdListState createState() => _CitiesIdListState();
}

class _CitiesIdListState extends State<CitiesIdList> {
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
				Navigator.pushAndRemoveUntil(
        context,
        MaterialPageRoute(builder: (context) => Home()),
  (Route<dynamic> route) => false,
);
      },
      child: Padding(
        padding: const EdgeInsets.all(10.0),
        child: Row(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.all(10.0),
              child: Text(
                widget.cityId['city_name'],
                style: TextStyle(
                  fontSize: 18.0,
                  fontWeight: FontWeight.w500,
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
