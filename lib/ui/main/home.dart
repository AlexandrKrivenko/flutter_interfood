import 'package:flutter/material.dart';
import '../places/places.dart';
import '../cart/shopping_cart.dart';
import 'package:edok/ui/promo/promo_main.dart';
import '../profile.dart';


class Home extends StatefulWidget {
  const Home ({
    Key key, 

  }) : super(key: key);

      
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  int currentTab = 0;
  PlacesView places;
  CartView cart;
  ProfileView profile;
  Widget currentPage;
  List<Widget> pages;

 
  @override
  void initState() {
    places = new PlacesView();
    cart = new CartView();
   
    profile = new ProfileView();
    pages = [places, cart,  profile];
    currentPage = places;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: currentPage,
      bottomNavigationBar: BottomNavigationBar(
        onTap: (index) {
          setState(() {
            currentTab = index;
            currentPage = pages[index];
          });
        },

        currentIndex: currentTab,
        items: <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(
              Icons.place,
              color: Colors.grey,
            ),
            title: Text('', style: TextStyle(color: Colors.black),),
            activeIcon: Icon(
              Icons.place,
              color: Colors.deepOrange,
            ),
          ),
          BottomNavigationBarItem(
            icon: Icon(
              Icons.shopping_cart,
              color: Colors.grey,
            ),
            title: Text('', style: TextStyle(color: Colors.black),),
            activeIcon: Icon(
              Icons.shopping_cart,
              color: Colors.deepOrange,
            ),
          ),
         
          BottomNavigationBarItem(
            icon: Icon(
              Icons.person,
              color: Colors.grey,
            ),
            title: Text('', style: TextStyle(color: Colors.black),),
            activeIcon: Icon(
              Icons.person,
              color: Colors.deepOrange,
            ),
          ),
        ],
      ),
    );
  }
}
