import 'package:flutter/material.dart';


class Category extends StatefulWidget {
  @override
  CategoryState createState() => new CategoryState();
}

class CategoryState extends State<Category> {
  Map<String, bool> values = {
    'Пиццы': false,
    'Роллы': false,
    'ФастФуд' : false,
    'Пасты' : false,
    'Суши' : false,
    'Напитки' : false,
    'Шашлыки' : false,
    'Салаты' : false,
    'Закуски' : false,

  };



  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      bottomNavigationBar: BottomAppBar(
        child: new Row(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[

            Container(
              height: 50,
              width: 195,
              child: InkWell(
                onTap: () {},
                child: Container(
                     child: Text ("Очистить",textAlign: TextAlign.center,
                              style:
                              TextStyle(
                                  color: Colors.black,
                                  fontSize: 20,
                                  fontWeight: FontWeight.w600
                              ),)

                ),
              ),
            ),


            Container(
              height: 50,
              width: 195,
              child: InkWell(
                onTap: () {},
                child: Container(
                          child: Text("Сохранить", textAlign: TextAlign.center,
                          style:
                          TextStyle(
                              color: Colors.deepOrange,
                              fontSize: 20,
                              fontWeight: FontWeight.w600
                          ),
                        ),
                ),
              ),
            ),
          ],),
            ),



      appBar: AppBar(
        title: Row(
          children: <Widget>[
            Padding(
              padding: EdgeInsets.only(right: 90),
            ),
            Container(
              child: Text(
                'Категории',
                style: TextStyle(
                  color: Colors.black,
                  fontSize: 18,
                  fontWeight: FontWeight.w700,
                ),
              ),
            ),
          ],
        ),
        centerTitle: true,
        iconTheme: IconThemeData(color: Colors.black),
        backgroundColor: Colors.white,
        brightness: Brightness.light,
        elevation:5,
      ),
      body: new ListView(
        children: values.keys.map((String key) {
          return new CheckboxListTile(
            activeColor: Colors.deepOrange,
            controlAffinity: ListTileControlAffinity.leading,
            title: new Text(key),
            value: values[key],
            onChanged: (bool value) {
              setState(() {
                values[key] = value;
              });
            },
          );
        }).toList(),
      ),

    );
  }

}

