import 'dart:convert';
import 'dart:async';
import 'package:http/http.dart' as http;

main() {
	getPlaces(1);
}

class Place {
  final String title;
  final String description;
  final String placeName;
  final String placePic;
  final String picture;

  Place.fromJson(Map jsonMap)
      : title = jsonMap['title'],
        description = jsonMap['description'],
        placeName = jsonMap['name'],
        placePic = jsonMap['logotype'],
        picture = jsonMap['logo'];

  String toString() => "${placeName}";
}

getPlaces(int cityId) async {
  var url = "https://edok.kz/api/rest/actions?id_city=$cityId";
  var apiAuth = "mp@admplaces";
  var apiKey = "mp@7uf98HKHf";

  Map<String, String> headers = {
    "X-REST-USERNAME": apiAuth,
    "X-REST-PASSWORD": apiKey
  };

  final client = new http.Client();
  var req = new http.Request(
    'get',
    Uri.parse(url),
  );
  req.headers['X-REST-USERNAME'] = apiAuth;
  req.headers['X-REST-PASSWORD'] = apiKey;

  var streamRes = await client.send(req);

  return streamRes.stream
      .transform(Utf8Decoder())
      .transform(JsonDecoder())
      .expand((jsonBody) => (jsonBody as Map)['places'])
      .map((jsonMap) => new Place.fromJson(jsonMap))
      .listen((data) => print(data))
      .onDone(() => client.close());
}
