import 'package:flutter/material.dart';
import 'dart:async';
import 'package:http/http.dart' as http;
import 'dart:convert';


class PromoView extends StatefulWidget {
  const PromoView({
    Key key,
    @required this.promoId
}) : super(key: key);
final Map promoId;
  @override
  _PromoViewState createState() => _PromoViewState();
}
class _PromoViewState extends State<PromoView> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Акции',
          style: TextStyle(
              color: Colors.black, fontSize: 14, fontWeight: FontWeight.w700),
        ),
        centerTitle: true,
        backgroundColor: Colors.white,
        brightness: Brightness.light,
        elevation: 0.50,
      ),
      body: new PromoItem(promoItemId: widget.promoId,),
    );
  }
}

class PromoItem extends StatefulWidget {
  const PromoItem({
    Key key,
    @required this.promoItemId
  }) : super(key: key);
  final Map promoItemId;

  @override
  _PromoItemState createState() => _PromoItemState();
}
class _PromoItemState extends State<PromoItem> {
  Map places;
  List PromoList;

  Future getData() async {
    http.Response response = await http
        .get("https://edok.kz/api/rest/actions?id_city=${widget.promoItemId['id']}",
        headers: {
          "x-rest-username": "mp@admplaces",
          "x-rest-password": "mp@7uf98HKHf"
        });
    places = json.decode(response.body);
    setState(() {
      PromoList = places["places"];
    });
  }
  @override
  void initState() {
    super.initState();
    getData();
  }
  @override
  Widget build(BuildContext context) {
    return Container(
        height: 660,
        child:    ListView.builder(
            itemCount:   PromoList == null ? 0 : PromoList.length,
            itemBuilder: (BuildContext context, int index) {
              return new PromoItems(promos: PromoList[index]);
            }));
  }
}

class PromoItems extends StatefulWidget {
  const PromoItems({
    Key key,
    @required this.promos
}) : super (key: key);
  final Map promos;


  @override
  _PromoItemsState createState() => _PromoItemsState();
}
class _PromoItemsState extends State<PromoItems> {
  Map promos;
  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 3,
      child: Column(
          children: <Widget>[
            Container(
              padding: EdgeInsets.only(bottom: 20,top: 10),
              child: Row(
                children: <Widget>[
                  Padding(
                    padding: EdgeInsets.all(10),
                  ),
                  Container(
                    padding: EdgeInsets.only(right: 20),
                    child: CircleAvatar(
                      backgroundColor: Colors.white,
                      child: Image.network(
                          'https://edok.kz/images/placeslogo/thumb/${widget.promos['logotype']}'
                      ),
                    ),
                  ),
                  Expanded(
                                      child: Text(
                      widget.promos['name'],
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(fontSize: 18, fontWeight: FontWeight.w700),
                    ),
                  ),
                ],
              ),
            ),
            Container(
              alignment: Alignment.centerLeft,
              margin: EdgeInsets.fromLTRB(20, 20, 0, 10),
              child: Text(
                '${widget.promos['actions'][0]['title']}',
                style: TextStyle(fontSize: 20, fontWeight: FontWeight.w700),
              ),
            ),
            Container(
              alignment: Alignment.centerLeft,
              margin: EdgeInsets.all(10),
              child: Text(
                '${widget.promos['actions'][0]['description']}',
                textAlign: TextAlign.justify,
              ),
            )
          ]),
    );
  }
}

class PromosDescription extends StatefulWidget {
  const PromosDescription({
    Key key,
    @required this.promo,
}) : super(key: key);
  final Map promo;
  @override
  _PromosDescriptionState createState() => _PromosDescriptionState();
}

class _PromosDescriptionState extends State<PromosDescription> {
  @override
  Widget build(BuildContext context) {
    return Container(
    child: PromosDesc(promoBody: widget.promo,),
    );
  }
}

class PromosDesc extends StatefulWidget {
  const PromosDesc({
    Key key,
    @required this.promoBody
}): super(key: key);
  final Map promoBody;
  @override
  _PromosDescState createState() => _PromosDescState();
}

class _PromosDescState extends State<PromosDesc> {
  @override
  Widget build(BuildContext context) {
    return Column(

    );
  }
}
