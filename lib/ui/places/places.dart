import 'package:edok/bubble_tab_indicator.dart';
import 'package:edok/ui/places/first_dishes.dart';
import 'package:edok/ui/places/second_dishes.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import '../place/place.dart';
import 'garniers.dart';


class PlacesView extends StatefulWidget {
  const PlacesView({
    Key key,
    @required this.changes
  }):super(key: key);
  final Map changes;
  

  @override
  _PlacesViewState createState() {
    return _PlacesViewState();
  }
}
class _PlacesViewState extends State<PlacesView> {
   @override
  void dispose() {
    super.dispose();
  }
  @override
  Widget build(BuildContext context) {
      return MaterialApp(
        debugShowCheckedModeBanner: false,
        home: DefaultTabController(
                    length: 3,
          child: Scaffold(
 appBar: PreferredSize(
          preferredSize: Size.fromHeight(90),
      child:
       AppBar(
         elevation: 5,
         bottom: TabBar(
         indicator: BubbleTabIndicator(
             indicatorHeight: 25.0,
           
             indicatorColor: Colors.orange,
             tabBarIndicatorSize: TabBarIndicatorSize.tab,
         ),
           tabs: <Widget>[
             Container(
               padding: EdgeInsets.only(bottom: 15),
               child: Text('Первые блюда', 
               style: TextStyle(
                 color: Colors.black38,
                 fontSize: 13
                 ),),
             ),
             Container(
               padding: EdgeInsets.only(bottom: 15),
               child: Text('Вторые блюда',
                style: TextStyle(
                  color: Colors.black38,
                fontSize: 13),),
             ),
             Container(
               padding: EdgeInsets.only(bottom: 15),
               child: Text('Гарниры',
                style: TextStyle(
                  color: Colors.black38,
                fontSize: 13),),
             ),
           ],
         ),
        title: Row(
          children:<Widget>[
            Container(
              padding: EdgeInsets.only(right: 10),
              child: Text(
                'Меню',
                style: TextStyle(
                  color: Colors.black,
                  fontSize: 30,
                  fontWeight: FontWeight.w700,
                ),
                textAlign: TextAlign.center,
              ),
            ),
            
          ],
        ),

        iconTheme: IconThemeData(color: Colors.black),
        backgroundColor: Colors.white,
        brightness: Brightness.light,
            )),
      body: TabBarView(
        children: <Widget>[
          FirstDishes(),
          SecondDishes(),
          Garniers(), 
        ],
        
      )
                  ),
     
    ));
  }
}

class PlacesList extends StatefulWidget {


    
     @override
  _PlacesListState createState() {
    return _PlacesListState();
  }}

class _PlacesListState extends State<PlacesList> {
    
  @override
  void dispose() {
    super.dispose();
  }
  @override
  Widget build(BuildContext context) {
    return ListView(
      children: <Widget>[
        new PlaceCard(),
      ],
    );
  }
}




class PlaceCard extends StatefulWidget {
  const PlaceCard ({
    Key key,

  }) : super(key: key);

  @override
  _PlaceCardState createState() => _PlaceCardState();
}
class _PlaceCardState extends State<PlaceCard> {






  @override
  Widget build(BuildContext context) {
     return         
    Container(
       );
             }
}

class PlaceItem extends StatefulWidget {
  const PlaceItem({
    Key key,
    @required this.Place,
  }) : super(key: key);
  final Map Place;
  @override
  _PlaceItemState createState() => _PlaceItemState();
}
class _PlaceItemState extends State<PlaceItem> {
  Map Place;
  @override
  Widget build(BuildContext context) {
    return Card(
      child: GestureDetector(
          onTap: () {
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => PlaceView(place: widget.Place),
              ),
            );
          },
          child: Column(
            children: <Widget>[
              Row(
                children: <Widget>[
                     Container(
                       padding: EdgeInsets.only(left: 10, top: 10),
                       child: Container(
                         width: 250,
                         child: Text(
                          widget.Place["name"],
                          overflow: TextOverflow.ellipsis,
                          style: TextStyle(fontSize: 18, fontWeight: FontWeight.w600),
                    ),
                       ),
                     ),
                  
                  Spacer(),
                  Container(
                    padding: EdgeInsets.only(right: 5),
                    child: Row(
                      children: <Widget>[
                        Text(
                      widget.Place['rating'],
                      style: TextStyle(fontSize: 20),
                    ),
                    Padding(
                      padding: EdgeInsets.all(1),
                    ),
                    Icon(
                      Icons.star,
                      color: Colors.deepOrange,
                    ),
                    Padding(
                      padding: EdgeInsets.all(5),
                    )
                      ],
                    ),
                  ),
                  
                ],
              ),
              PlaceMenuItems(dishes: widget.Place['dish']),
              Container(
                padding: EdgeInsets.fromLTRB(15, 5, 5, 5),
                child: Row(
                  children: <Widget>[
                    Text(
                      'Европейская',
                      style: TextStyle(color: Colors.grey),
                    ),
                    Text(
                      ' - Итальянская - ',
                      style: TextStyle(color: Colors.grey),
                    ),
                    Text(
                      'Азиатская',
                      style: TextStyle(color: Colors.grey),
                    )
                  ],
                ),
              )
            ],
          ),
      )
    );
  }
}



class PlaceMenuItems extends StatefulWidget {
  const PlaceMenuItems({
    Key key,
    @required this.dishes
  }) : super(key: key);
  final List dishes;
  @override
  _PlaceMenuItemsState createState() => _PlaceMenuItemsState();
}
class _PlaceMenuItemsState extends State<PlaceMenuItems> {
  List dishes;
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(10),
      height: 200,
      child: widget.dishes.isEmpty ? Center(
        child: Text('ЗАВЕДЕНИЕ ЗАКРЫТО',style: TextStyle(
          fontSize: 18,
        ),)) : ListView.builder(
          scrollDirection: Axis.horizontal,
          itemCount: widget.dishes == null ? 0 : widget.dishes.length,
          itemBuilder: (BuildContext context, int index) {
            return new PlaceMenuItem(dish: widget.dishes[index]);
          }),
    );
  }
}


class PlaceMenuItem extends StatefulWidget {
  const PlaceMenuItem({
    Key key,
    @required this.dish
  }) : super(key: key);
final Map dish;
  @override
  _PlaceMenuItemState createState() => _PlaceMenuItemState();
}

class _PlaceMenuItemState extends State<PlaceMenuItem> {
  Map dish;
  @override
  Widget build(BuildContext context) {
    return Container(
        child: Column(
          children: <Widget>[
            SizedBox(
              width: 150,
              height: 125,
              child: Container(
                padding: EdgeInsets.only(left: 10),
                child: Image.network(
                    'https://edok.kz/images/food_photo/${widget.dish["photo"]}'
                ),
              ),
            ),
            Container(
              width: 150,
              padding: EdgeInsets.all(5),
              child: Text(
                widget.dish['name'],
                overflow: TextOverflow.ellipsis,
                style: TextStyle(fontWeight: FontWeight.w700),
              ),
            ),
            Container(
              child: Row(
                children: <Widget>[
                  Container(
                    padding: EdgeInsets.fromLTRB(0, 5, 2, 0),
                    child: Text(
                      widget.dish['price'],
                      textAlign: TextAlign.left,
                      style: TextStyle(color: Colors.deepOrange, fontSize: 15),
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.fromLTRB(0, 5, 100, 0),
                    child: Text(
                      "₸",
                      style: TextStyle(color: Colors.deepOrange, fontSize: 15),
                    ),
                  )
                ],
              ),
            ),
          ],
        ));
  }
}