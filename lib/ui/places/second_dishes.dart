import 'package:flutter/material.dart';

class SecondDishes extends StatefulWidget {
  @override
  _SecondDishesState createState() => _SecondDishesState();
}

class _SecondDishesState extends State<SecondDishes> {
  @override
  Widget build(BuildContext context) {
    return ListView(
      children: <Widget>[
        SecondDishCard(),
        SecondDishCard(),
        SecondDishCard(),
        SecondDishCard(),
        SecondDishCard(),
        SecondDishCard(),
      
      ],
    );
  }
}


class SecondDishCard extends StatefulWidget {
  @override
  _SecondDishCardState createState() => _SecondDishCardState();
}

class _SecondDishCardState extends State<SecondDishCard> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: <Widget>[
            SecondDishesCards(),
            SecondDishesCards(),
        ],
      ),
    );
  }
}

class SecondDishesCards extends StatefulWidget {
  @override
  _SecondDishesCardsState createState() => _SecondDishesCardsState();
}

class _SecondDishesCardsState extends State<SecondDishesCards> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: <Widget>[
            Container(
              height: 200,
              width: 165,
              child: Image(
                image: new AssetImage("assets/second.jpg"),
        ),
            ),
            Container(
               padding: EdgeInsets.only(top: 10),
              child: Text('Шашлык куринный', style:TextStyle(fontSize: 14)
              ),
            ),
            Container(
               padding: EdgeInsets.only(top: 10),
              child: Text('640 ₸', style:TextStyle(fontSize: 14))),
             Container(
               padding: EdgeInsets.only(bottom: 10, top: 10),
             child: Container(
          width: 100,
          height: 25,
          padding: EdgeInsets.all(1),
               decoration: new BoxDecoration(
               color: Colors.orange,
                 borderRadius: BorderRadius.circular(30)),
                 child: Container(
                  
                   child: new MaterialButton(
                     shape: new RoundedRectangleBorder(
                       borderRadius: new BorderRadius.circular(30.0)),
                       color: Colors.white,
                        child: new Text(
                            'В корзину ',
                                style: TextStyle(fontSize: 13.0, color: Colors.orange),
                         ),
                            onPressed: () => {},
                    ),
                 ),
               
    ),
             ),
        ])
    );
  }
}