import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;



class FirstDishes extends StatefulWidget {
  @override
  _FirstDishesState createState() => _FirstDishesState();
}

class _FirstDishesState extends State<FirstDishes> {
  @override
  Widget build(BuildContext context) {
    return ListView(
      children: <Widget>[
        FirstDishCard(),
        FirstDishCard(),
        FirstDishCard(),
        FirstDishCard(),
        FirstDishCard(),
        FirstDishCard(),
      
      ],
    );
  }
}


class FirstDishCard extends StatefulWidget {
  @override
  _FirstDishCardState createState() => _FirstDishCardState();
}

class _FirstDishCardState extends State<FirstDishCard> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: <Widget>[
            FirstDishesCards(),
            FirstDishesCards(),
        ],
      ),
    );
  }
}

class FirstDishesCards extends StatefulWidget {
  @override
  _FirstDishesCardsState createState() => _FirstDishesCardsState();
}

class _FirstDishesCardsState extends State<FirstDishesCards> {
   Map places;
  List PlacesList;
  Future getData() async {
    http.Response response = await http.get(
        "https://edok.kz/api/rest/placesv2?id_city=1",
        headers: {
      "isd": "tVHL7m2ZieExtJovi9mPRxxdDKPQWdBMtdQgFaDIESbOTvbpTuV08La5ioHwXlCE",
    });
    places = json.decode(response.body);
    setState(() {
      PlacesList = places["places"];
    });
  }
  @override
  void initState() {
    super.initState();
    getData();
  }
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: <Widget>[
            Container(
              height: 200,
              width: 165,
              child: Image(
                image: new AssetImage("assets/first.jpg"),
        ),
            ),
            Container(
               padding: EdgeInsets.only(top: 10),
              child: Text('Борщ по-украински', style:TextStyle(fontSize: 14)
              ),
            ),
            Container(
               padding: EdgeInsets.only(top: 10),
              child: Text('410 ₸', style:TextStyle(fontSize: 14))),
             Container(
               padding: EdgeInsets.only(bottom: 10, top: 10),
             child: Container(
          width: 100,
          height: 25,
          padding: EdgeInsets.all(1),
               decoration: new BoxDecoration(
               color: Colors.orange,
                 borderRadius: BorderRadius.circular(30)),
                 child: Container(
                  
                   child: new MaterialButton(
                     shape: new RoundedRectangleBorder(
                       borderRadius: new BorderRadius.circular(30.0)),
                       color: Colors.white,
                        child: new Text(
                            'В корзину ',
                                style: TextStyle(fontSize: 13.0, color: Colors.orange),
                         ),
                            onPressed: () => {},
                    ),
                 ),
               
    ),
             ),
        ])
    );
  }
}