
import 'package:flutter/material.dart';


class ProfileView extends StatefulWidget {
  @override
  _ProfileViewState createState() => _ProfileViewState();
}

class _ProfileViewState extends State<ProfileView> {


   String uid = '';

  getUid() {}


  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Профиль',
          style: TextStyle(
              color: Colors.black, fontSize: 14, fontWeight: FontWeight.w700),
        ),
        centerTitle: true,
        backgroundColor: Colors.white,
        brightness: Brightness.light,
        elevation: 0.50,
      ),
      body: Container(
        child: ListView(
          children: <Widget>[
            Card(
              child: ListTile(
                leading: Icon(Icons.person),
                title: Text(
                  'Пользователь',
                  style: TextStyle(fontSize: 14, fontWeight: FontWeight.w700),
                ),
                subtitle: Text('$uid'),
              ),
              color: Colors.white,
            ),
            Padding(
              padding: const EdgeInsets.all(20),
            ),
            Card(
              shape: BeveledRectangleBorder(),
              margin: EdgeInsets.all(0.4),
              child: ListTile(
                title: Text(
                  'История заказов',
                  style: TextStyle(fontSize: 14),
                ),
                onTap: () {
                  var router = new MaterialPageRoute(
                      builder: (BuildContext context) => new _HistoryView());
                  Navigator.of(context).push(router);
                },
                trailing: Icon(
                  Icons.arrow_right,
                  color: Colors.black,
                ),
              ),
            ),
            Card(
              shape: BeveledRectangleBorder(),
              margin: EdgeInsets.all(0.4),
              child: ListTile(
                title: Text(
                  'Выйти из профиля',
                  style: TextStyle(fontSize: 14),
                ),
                onTap: () {
                },
              ),
            ),
          ],
        ),
      ),
      backgroundColor: Colors.grey.shade100,
    );
  }
}

class _HistoryView extends StatefulWidget {
  @override
  __HistoryViewState createState() => __HistoryViewState();
}

class __HistoryViewState extends State<_HistoryView> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'История заказов',
          style: TextStyle(
              color: Colors.black, fontSize: 14, fontWeight: FontWeight.w700),
        ),
        centerTitle: true,
        backgroundColor: Colors.white,
        brightness: Brightness.light,
        iconTheme: IconThemeData(color: Colors.black),
        elevation: 0,
      ),
      body: Container(
        child: Text('History container'),
        alignment: Alignment.center,
      ),
    );
  }
}
