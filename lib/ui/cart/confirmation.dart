import 'package:flutter/material.dart';

class ConfirmationView extends StatefulWidget {
  @override
  _ConfirmationViewState createState() => _ConfirmationViewState();
}

class _ConfirmationViewState extends State<ConfirmationView> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Корзина',
          style: TextStyle(
              color: Colors.black, fontSize: 14, fontWeight: FontWeight.w700),
        ),
        centerTitle: true,
        iconTheme: IconThemeData(color: Colors.black),
        backgroundColor: Colors.white,
        brightness: Brightness.light,
        elevation: 0.50,
      ),
      body: ConfirmationBody(),
      backgroundColor: Colors.white,
      bottomNavigationBar: ButtonBar(
        alignment: MainAxisAlignment.center,
        children: <Widget>[
          Container(
            width: 150,
            child: FlatButton(
              onPressed: () {
                Navigator.pop(context);
              },
              child: Text('Назад'),
            ),
          ),
          Container(
            width: 150,
            child: FlatButton(
              onPressed: () => debugPrint('confirmed'),
              child: Text('Оформить заказ'),
              textColor: Colors.deepOrange,
            ),
          ),
        ],
      ),
    );
  }
}

class ConfirmationBody extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ListView(
      children: <Widget>[
        Container(
          margin: EdgeInsets.fromLTRB(25, 20, 0, 10),
          child: Text('ИТОГ'),
          alignment: Alignment.centerLeft,
        ),
        Divider(),
        Padding(
          padding: const EdgeInsets.all(10.0),
          child: Column(
            children: <Widget>[
              Container(
                margin: EdgeInsets.all(15),
                child: Row(
                  children: <Widget>[
                    Text(
                      'Заказ',
                      style: TextStyle(fontWeight: FontWeight.w700),
                    ),
                    Spacer(),
                    Text('10 150₸'),
                  ],
                ),
              ),
              Divider(),
              Container(
                margin: EdgeInsets.all(15),
                child: Row(
                  children: <Widget>[
                    Text(
                      'Доставка',
                      style: TextStyle(fontWeight: FontWeight.w700),
                    ),
                    Spacer(),
                    Text('0₸'),
                  ],
                ),
              ),
              Divider(),
            ],
          ),
        ),
        Container(
          margin: EdgeInsets.fromLTRB(25, 0, 25, 10),
          child: Row(
            children: <Widget>[
              Text(
                'Итого:',
                style: TextStyle(fontSize: 18),
              ),
              Spacer(),
              Text(
                '10 150₸',
                style: TextStyle(
                    color: Colors.deepOrange,
                    fontSize: 20,
                    fontWeight: FontWeight.w700),
              ),
            ],
          ),
        ),
        Divider(),
        Container(
          alignment: Alignment.bottomCenter,
          margin: EdgeInsets.all(10),
          height: 230,
          child: Text(
            'Нажимая кнопку "Оформить заказ" Вы автоматически соглашаетесь с условиями пользовательского соглашения',
            style: TextStyle(fontSize: 12),
            textAlign: TextAlign.justify,
          ),
        ),
      ],
    );
  }
}
