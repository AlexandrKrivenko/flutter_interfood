import 'package:flutter/material.dart';
import 'confirmation.dart';

class DeliveryAddress extends StatefulWidget {
  @override
  _DeliveryAddressState createState() => _DeliveryAddressState();
}

class _DeliveryAddressState extends State<DeliveryAddress> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Корзина',
          style: TextStyle(
              color: Colors.black, fontSize: 14, fontWeight: FontWeight.w700),
        ),
        centerTitle: true,
        iconTheme: IconThemeData(color: Colors.black),
        backgroundColor: Colors.white,
        brightness: Brightness.light,
        elevation: 0.50,
      ),
      body: DeliveryAddressBody(),
      bottomNavigationBar: ButtonBar(
        alignment: MainAxisAlignment.center,
        children: <Widget>[
          Container(
            width: 150,
            child: FlatButton(
              onPressed: () {
                Navigator.pop(context);
              },
              child: Text('Назад'),
            ),
          ),
          Container(
            width: 150,
            child: FlatButton(
              onPressed: () {
                var route = new MaterialPageRoute(
                    builder: (BuildContext context) => ConfirmationView());
                return Navigator.of(context).push(route);
              },
              child: Text('Далее'),
              textColor: Colors.deepOrange,
            ),
          ),
        ],
      ),
      backgroundColor: Colors.white,
    );
  }
}

class DeliveryAddressBody extends StatefulWidget {
  @override
  _DeliveryAddressBodyState createState() => _DeliveryAddressBodyState();
}

class _DeliveryAddressBodyState extends State<DeliveryAddressBody> {
  var _streetController = new TextEditingController();

  @override
  Widget build(BuildContext context) {
    return ListView(
      children: <Widget>[
        Container(
          margin: EdgeInsets.fromLTRB(25, 20, 0, 10),
          child: Text('ДОСТАВКА'),
          alignment: Alignment.centerLeft,
        ),
        Divider(),
        Container(
          padding: EdgeInsets.fromLTRB(25, 0, 25, 0),
          child: Column(
            children: <Widget>[
              TextField(
                decoration: InputDecoration(
                    labelText: 'Улица',
                    border: UnderlineInputBorder(
                        borderSide:
                            BorderSide(color: Colors.deepOrangeAccent))),
                controller: _streetController,
              ),
              TextField(
                decoration: InputDecoration(labelText: 'Дом'),
              ),
              TextField(
                decoration: InputDecoration(labelText: 'Квартира'),
              ),
              TextField(
                decoration: InputDecoration(labelText: 'Подъезд'),
              ),
              TextField(
                decoration: InputDecoration(labelText: 'Этаж'),
              ),
              TextField(
                decoration: InputDecoration(labelText: 'Кол-во персон'),
              ),
              TextField(
                decoration: InputDecoration(labelText: 'Код домофона'),
              ),
              TextField(
                decoration: InputDecoration(labelText: 'Комментарий'),
              ),
            ],
          ),
        )
      ],
    );
  }
}
