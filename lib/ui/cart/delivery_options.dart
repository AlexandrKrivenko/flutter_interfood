import 'package:flutter/material.dart';
import 'delivery_address.dart';

class DeliveryOptions extends StatefulWidget {
  @override
  _DeliveryOptionsState createState() => _DeliveryOptionsState();
}

class _DeliveryOptionsState extends State<DeliveryOptions> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Корзина',
          style: TextStyle(
              color: Colors.black, fontSize: 14, fontWeight: FontWeight.w700),
        ),
        centerTitle: true,
        iconTheme: IconThemeData(color: Colors.black),
        backgroundColor: Colors.white,
        brightness: Brightness.light,
        elevation: 0.50,
      ),
      body: DeliveryOptionsBody(),
      bottomNavigationBar: ButtonBar(
        alignment: MainAxisAlignment.center,
        children: <Widget>[
          Container(
            width: 150,
            child: FlatButton(
              onPressed: () {
                Navigator.pop(context);
              },
              child: Text('Назад'),
            ),
          ),
          Container(
            width: 150,
            child: FlatButton(
              onPressed: () {
                var route = new MaterialPageRoute(
                    builder: (BuildContext context) => DeliveryAddress());
                return Navigator.of(context).push(route);
              },
              child: Text('Далее'),
              textColor: Colors.deepOrange,
            ),
          ),
        ],
      ),
    );
  }
}

class DeliveryOptionsBody extends StatelessWidget {
  const DeliveryOptionsBody({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView(
      children: <Widget>[
        Container(
          margin: EdgeInsets.fromLTRB(25, 20, 0, 10),
          child: Text('ДОСТАВКА'),
          alignment: Alignment.centerLeft,
        ),
        Divider(),
        Container(
          padding: EdgeInsets.all(10),
          child: Column(
            children: <Widget>[
              RadioListTile(
                value: 1,
                groupValue: null,
                onChanged: null,
                activeColor: Colors.deepOrange,
                title: Text(
                  'Доставка',
                  style: TextStyle(
                      color: Colors.black,
                      fontWeight: FontWeight.w500,
                      fontSize: 13),
                ),
              ),
              Divider(),
              RadioListTile(
                value: 2,
                groupValue: null,
                onChanged: null,
                activeColor: Colors.deepOrange,
                title: Text(
                  'Самовывоз',
                  style: TextStyle(
                      color: Colors.black,
                      fontWeight: FontWeight.w500,
                      fontSize: 13),
                ),
              ),
            ],
          ),
        ),
        Divider(),
        Container(
          margin: EdgeInsets.fromLTRB(25, 20, 0, 10),
          child: Text('ВАШ РАЙОН'),
          alignment: Alignment.centerLeft,
        ),
        Divider(),
        Container(
          padding: EdgeInsets.all(10),
          child: Column(
            children: <Widget>[
              RadioListTile(
                value: 1,
                groupValue: null,
                onChanged: null,
                activeColor: Colors.deepOrange,
                title: Container(
                  child: Row(
                    children: <Widget>[
                      Text(
                        'Город',
                        style: TextStyle(
                            color: Colors.black,
                            fontWeight: FontWeight.w500,
                            fontSize: 13),
                      ),
                      Spacer(),
                      Text(
                        '350тг.',
                        style: TextStyle(
                            color: Colors.black,
                            fontWeight: FontWeight.w600,
                            fontSize: 13),
                      )
                    ],
                  ),
                ),
                subtitle: Text('Мин. сумма заказа: 350тг. Бесплатно: 1990тг'),
              ),
              Divider(),
              RadioListTile(
                value: 2,
                groupValue: null,
                onChanged: null,
                activeColor: Colors.deepOrange,
                title: Container(
                  child: Row(
                    children: <Widget>[
                      Text(
                        'Юго-Восток',
                        style: TextStyle(
                            color: Colors.black,
                            fontWeight: FontWeight.w500,
                            fontSize: 13),
                      ),
                      Spacer(),
                      Text(
                        '350тг.',
                        style: TextStyle(
                            color: Colors.black,
                            fontWeight: FontWeight.w600,
                            fontSize: 13),
                      )
                    ],
                  ),
                ),
                subtitle: Text('Мин. сумма заказа: 350тг. Бесплатно: 1990тг'),
              ),
              Divider(),
              RadioListTile(
                value: 2,
                groupValue: null,
                onChanged: null,
                activeColor: Colors.deepOrange,
                title: Container(
                  child: Row(
                    children: <Widget>[
                      Text(
                        'Михайловка',
                        style: TextStyle(
                            color: Colors.black,
                            fontWeight: FontWeight.w500,
                            fontSize: 13),
                      ),
                      Spacer(),
                      Text(
                        '350тг.',
                        style: TextStyle(
                            color: Colors.black,
                            fontWeight: FontWeight.w600,
                            fontSize: 13),
                      )
                    ],
                  ),
                ),
                subtitle: Text('Мин. сумма заказа: 350тг. Бесплатно: 1990тг'),
              ),
              Divider(),
              RadioListTile(
                value: 2,
                groupValue: null,
                onChanged: null,
                activeColor: Colors.deepOrange,
                title: Container(
                  child: Row(
                    children: <Widget>[
                      Text(
                        'Майкудук',
                        style: TextStyle(
                            color: Colors.black,
                            fontWeight: FontWeight.w500,
                            fontSize: 13),
                      ),
                      Spacer(),
                      Text(
                        '350тг.',
                        style: TextStyle(
                            color: Colors.black,
                            fontWeight: FontWeight.w600,
                            fontSize: 13),
                      )
                    ],
                  ),
                ),
                subtitle: Text(
                  'Мин. сумма заказа: 350тг. Бесплатно: 1990тг',
                  style: TextStyle(
                    fontSize: 12,
                  ),
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }
}
