import 'package:edok/ui/cart/payment.dart';
import 'package:flutter/material.dart';
import '../profile.dart';
import 'delivery_options.dart';

class CartView extends StatefulWidget {
  @override
  _CartViewState createState() => _CartViewState();
}

class _CartViewState extends State<CartView> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
    bottomNavigationBar: BottomAppBar(
          color: Colors.orange,
          child: new Container(
            child: MaterialButton(
                height: 60,
                onPressed: () {
                  Navigator.push(
                  context,
                       MaterialPageRoute(builder: (context) => Payment()));
                },
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text(
                      "Перейти к оплате",
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: 16,
                          fontWeight: FontWeight.w600),
                    ),
                    new Icon(Icons.keyboard_arrow_right, color: Colors.white,)
                  ],
                )),
          )),
          appBar: PreferredSize(
          preferredSize: Size.fromHeight(90),
      child:
       AppBar(
         
         elevation: 5,
         backgroundColor: Colors.white,
        title: Row(
          children:<Widget>[
            Container(
              padding: EdgeInsets.only(right: 10),
              child: Text(
                'Мой заказ',
                style: TextStyle(
                  color: Colors.black,
                  fontSize: 30,
                  fontWeight: FontWeight.w700,
                ),
                textAlign: TextAlign.center,
              ),
            ),
          ],
        ),
       ),
    ),
    body: ListView(
      children: <Widget>[
          CardViewList(),
          CardVireBottomInfo(),
      ],
    ));
  }
}


class CardViewList extends StatefulWidget {
  @override
  _CardViewListState createState() => _CardViewListState();
}

class _CardViewListState extends State<CardViewList> {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        ShoppingCartItem(),
        ShoppingCartItem(),
        ShoppingCartItem(),
      ],
    );
  }
}

class ShoppingCartItem extends StatefulWidget {
  @override
  _ShoppingCartItemState createState() => _ShoppingCartItemState();
}

class _ShoppingCartItemState extends State<ShoppingCartItem> {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      padding: EdgeInsets.only(top: 3),
      child: Row(
        children: <Widget>[
          Container(
            padding: EdgeInsets.only(left: 3),
            child: SizedBox(
              height: 110,
              width: 130,
              child: Image(
                image: AssetImage('assets/second.jpg'),
              ),
            ),
          ),
          Stack(
            children: <Widget>[
              Container(
                padding: EdgeInsets.only(bottom: 80),
                child: Row(
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.only(left: 5),
                      child: Text('Шашлык куринный', style:TextStyle(fontSize: 15, fontWeight: FontWeight.w500))),
                    Container(
                      padding: EdgeInsets.only(right: 5, left: 65),
                      child: Text('640 ₸', style:TextStyle(fontSize: 15, fontWeight: FontWeight.w500))),
                  ],
                ),
              ),
              Row(
                
                children: <Widget>[
                  Container(
                    padding: EdgeInsets.only(top: 15, left: 230),
                    child: Text('x2', style:TextStyle(fontSize: 13, color: Colors.grey)))
                ],
              ),
              Container(
                padding: EdgeInsets.only(top: 50),
                child: Row(
                  children: <Widget>[
                    ItemCounter(),
                    Container(
                      padding: EdgeInsets.only(left: 70),
                      child: Text('1 280 ₸', style:TextStyle(fontSize: 17,)
                      ),
                    )],
                ),
              ),
            ],
          )
        ],
      ),
    );
  }
}


class CardVireBottomInfo extends StatefulWidget {
  
  @override
  _CardVireBottomInfoState createState() => _CardVireBottomInfoState();
}

class _CardVireBottomInfoState extends State<CardVireBottomInfo> {
int _currValue = 1;

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      child: Column(
        children: <Widget>[
          Row(
            children: <Widget>[
              Container(
                padding: EdgeInsets.only(left: 15),
                child: Text('Выберите опцию заказа',
                style: TextStyle(
                  fontSize: 15,
                  fontWeight: FontWeight.w500
                )),
              )
            ],
          ),
          Row(
            children: <Widget>[
              Radio(
                groupValue: _currValue,
                onChanged: (int i) => setState(() => _currValue = i),
                 value: 0,
                 activeColor: Colors.orange,
                   
              ),
              Text('В зале', style: TextStyle(
                fontSize: 15
              ),),
               Radio(
                groupValue: _currValue,
                onChanged: (int i) => setState(() => _currValue = i),
                 value: 1,
                 
                 activeColor: Colors.orange,
               ),
               Text('На вынос', style: TextStyle(
                fontSize: 15
              ),),],
          ),
          Container(
            padding: EdgeInsets.only(top: 15),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
              Container(
                padding: EdgeInsets.only(left: 15),
                child: Text('Итого: ', style: TextStyle(
                fontSize: 17
            ),),
              ),
            Container(
              padding: EdgeInsets.only(right: 10),
              child: Text('2 455 ₸', style:  TextStyle(
                color: Colors.orange,
                fontSize: 17,
                fontWeight: FontWeight.w600
              )),
            )
            ],),
          )
          
        ],
        
      ),
    );
  }
}


class ItemCounter extends StatefulWidget {
  ItemCounter({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _ItemCounterState createState() => _ItemCounterState();
}

class _ItemCounterState extends State<ItemCounter> {
  int _counter = 0;
  void _incrementCounter() {
    setState(() {
      _counter++;
    });
  }

  void _dicrementCounter() {
    setState(() {
      _counter--;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.center,
      decoration: new BoxDecoration(
          border: new Border.all(color: Colors.grey),
          borderRadius: BorderRadius.circular(20)),
      height: 40,
      width: 120,
      child: Row(
        children: <Widget>[
          Spacer(),
          IconButton(
            onPressed: _dicrementCounter,
            highlightColor: Colors.black,
            splashColor: Colors.black,
            icon: Icon(Icons.remove),
            iconSize: 20,
          ),
          Spacer(),
          Text(
            '$_counter',
            style: TextStyle(
                color: Colors.deepOrange,
                fontSize: 15,
                fontWeight: FontWeight.w500
            ),
            //style: Theme.of(context).textTheme.display1,

          ),
          Spacer(),
          IconButton(
              onPressed: _incrementCounter,
              highlightColor: Colors.black12,
              icon: Icon(Icons.add)

          ),
          Spacer(),
        ],
      ),
    );
  }
}


class _commentSection extends StatefulWidget {
  @override
  __commentSectionState createState() => __commentSectionState();
}

class __commentSectionState extends State<_commentSection> {
  var _commentTextController = new TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Divider(),
        Container(
          margin: EdgeInsets.fromLTRB(20, 5, 0, 0),
          alignment: Alignment.centerLeft,
          child: Text(
            'Пожелание к заказу',
            style: TextStyle(color: Colors.grey, fontSize: 12),
          ),
        ),
        Container(
          margin: EdgeInsets.fromLTRB(10, 0, 10, 0),
          child: TextField(
            controller: _commentTextController,
            decoration: InputDecoration(
                labelText: 'Добавьте пожелания к заказу или комментарий',
                labelStyle: TextStyle(fontSize: 13),
                contentPadding: EdgeInsets.all(10)),
          ),
          height: 200,
        )
      ],
    );
  }
}
