import 'package:flutter/material.dart';


class Payment extends StatefulWidget {
  @override
  _PaymentState createState() => _PaymentState();
}

class _PaymentState extends State<Payment> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
          preferredSize: Size.fromHeight(90),
      child:
       AppBar(
          elevation: 5,
         backgroundColor: Colors.white,
        title: Row(
          children:<Widget>[
            Container(
              padding: EdgeInsets.only(right: 10),
              child: Text(
                'Payment info',
                style: TextStyle(
                  color: Colors.black,
                  fontSize: 30,
                  fontWeight: FontWeight.w700,
                ),
                textAlign: TextAlign.center,
              ),
            ),
          ],
        ),
        iconTheme: IconThemeData(color: Colors.black),
      ),
    ),
    body: InputField(),);
  }
}

class InputField extends StatefulWidget {
  
  @override
  _InputFieldState createState() => _InputFieldState();
}

class _InputFieldState extends State<InputField> {
  bool _value = false;
  void _onChanged(bool value) {
    setState(() {
      _value = value;
    });
  }
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: <Widget>[
          Container(
            padding: EdgeInsets.only( top: 10, right: 200 , bottom: 20),
            child: Text('Данные по карте',style: TextStyle(
              fontSize: 15
            ),),
          ),
          Container(
            padding: EdgeInsets.only(left: 5),
            width: 370,
            height: 50,
            child: TextField(
              decoration: InputDecoration(
              hintText: ('Номер карты'),
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(10)
                )
              ),
              onChanged: (value) {
                setState(() {
                  
                });
              },
              
            ),
          ),

          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              Container(
            padding: EdgeInsets.only(left: 5, top: 10),
            width: 180,
            height: 50,
            child: TextField(
              decoration: InputDecoration(
              hintText: ('Действует до '),
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(10)
                )
              ),
              onChanged: (value) {
                setState(() {
                  
                });
              },
              
            ),
          ),
          Container(
            padding: EdgeInsets.only(top: 5),
            width: 180,
            height: 50,
            child: TextField(
              decoration: InputDecoration(
              hintText: ('CVV'),
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(10)
                )
              ),
              onChanged: (value) {
                setState(() {
                  
                });
              },
              
            ),
          ),
            ],
          ),
         
         Row(
           mainAxisAlignment: MainAxisAlignment.spaceBetween,
           children: <Widget>[
           Flexible(
             child: Container(
               padding: EdgeInsets.only(left: 20),
               child: Text('Я хочу сохранить карту для будущий покупок', style: TextStyle(fontSize: 15)))
             ,
           ),
           Container(
             padding: EdgeInsets.only(right: 5),
             child: Switch(
               value: _value,
                onChanged: (bool value) {_onChanged(value);},
               activeColor: Colors.orange,
             ),
           )
         ],)
          
        ],
      ),
    );
  }
}