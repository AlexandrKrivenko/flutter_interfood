import 'package:flutter/material.dart';
import 'tabs/comments.dart';
import 'tabs/promo.dart';
import 'tabs/about.dart';
import 'package:edok/ui/place/tabs/Menu/menulist.dart';
import 'package:edok/ui/place/tabs/Menu/trashbin.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';


class PlaceView extends StatefulWidget {
  const PlaceView({
    Key key,
    @required this.place
  }) : super(key: key);
  final Map place;

  @override
  _PlaceViewState createState() => _PlaceViewState();
}
class _PlaceViewState extends State<PlaceView>
    with SingleTickerProviderStateMixin {
  TabController controller;

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      resizeToAvoidBottomPadding: false,
      resizeToAvoidBottomInset: false,
      bottomNavigationBar: BottomAppBar(
          color: Colors.deepOrange,
          child: new Container(
            child: MaterialButton(
                height: 60,
                onPressed: () {
                },
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text(
                      "Добавить в корзину",
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: 16,
                          fontWeight: FontWeight.w600),
                    ),
                    new Icon(Icons.radio_button_unchecked)
                  ],
                )),
          )),
      appBar: AppBar(
        title: Text(
          widget.place['name'],
          style: TextStyle(
            color: Colors.black,
            fontSize: 16,
            fontWeight: FontWeight.w700,
          ),
          textAlign: TextAlign.center,
        ),
        actions: <Widget>[
          Container(
            child: IconButton(
              icon: Icon(Icons.search),
              onPressed: () {},
            ),
            padding: EdgeInsets.all(10),
          ),
        ],
        centerTitle: true,
        iconTheme: IconThemeData(color: Colors.black),
        backgroundColor: Colors.white,
        brightness: Brightness.light,
        elevation: 0,
      ),
      body: CustomScrollView(
        slivers: <Widget>[
          SliverAppBar(
            brightness: Brightness.light,
            expandedHeight: 110,
            floating: true,
            pinned: false,
            flexibleSpace:  Container(child: PlaceHeader(header: widget.place)),
            elevation: 10,
            leading: Container(),
          ),
          SliverAppBar(
            floating: true,
            pinned: true,
            backgroundColor: Colors.white,
            expandedHeight: 10,
            bottom: TabBar(
                controller: controller,
                indicatorColor: Colors.deepOrangeAccent,
                labelColor: Colors.black,
                tabs: <Widget>[
                  Tab(
                    text: 'Меню',
                  ),
                  Tab(
                    text: 'Акции',
                  ),
                  Tab(
                    text: 'Отзывы',
                  ),
                  Tab(
                    text: 'О заведении',
                  ),
                ]),
          ),
          SliverFillRemaining(
            child: TabBarView(controller: controller, children: <Widget>[
              Container(
                child: PlaceMenu(placeMenuList: widget.place),
              ),
              ListView(
                children: <Widget>[
                  PromoItem(promo: widget.place),
                ],
              ),
              ListView(
                children: <Widget>[
                  new CommentItem(),
                  new CommentItem(),
                  new CommentItem(),
                  new CommentItem(),
                ],
              ),
              ListView(
                children: <Widget>[AboutView(about: widget.place,)],
              ),
            ]),
          )
        ],
      ),
    );
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  @override
  void initState() {
    controller = new TabController(length: 4, vsync: this);
    super.initState();
  }
}


class PlaceHeader extends StatefulWidget {
  const PlaceHeader({
    Key key,
    @required this.header,
  }) : super(key: key);
  final Map header;
  @override
  _PlaceHeaderState createState() => _PlaceHeaderState();
}
class _PlaceHeaderState extends State<PlaceHeader> {
  @override
  Widget build(BuildContext context) {
    
    return Container(
  
        color: Colors.white,
        child: Row(
          children: <Widget>[
            Column(
              children: <Widget>[
                Container(
                  padding: EdgeInsets.all(10),
                  child: CircleAvatar(
                    backgroundColor: Colors.white,
                    child: Image.network('https://edok.kz/images/placeslogo/thumb/${widget.header['logotype']}'),
                    maxRadius: 40,
                  ),
                ),
              ],
            ),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Row(
                    children: <Widget>[
                      Container(
                        padding: EdgeInsets.only(top: 5, left: 5),
                        child: Container(
                          padding: EdgeInsets.only(left: 3),
                          width: 220,
                          child: Text(
                           widget.header['name'],
                            style: TextStyle(
                              fontSize: 19,
                              fontWeight: FontWeight.w600,
                            ),
                          ),
                        ),
                      ),
                      Spacer(),

                      Container(
                        padding: EdgeInsets.all(5),
                        child: Row(
                          children: <Widget>[
                            Text(
                              widget.header['rating'],
                              style: TextStyle(fontSize: 20),
                            ),
                            Icon(
                              Icons.star,
                              color: Colors.deepOrange,
                            ),
                          ],
                        ),
                      )
                    ],
                  ),
                  Container(
                    padding: EdgeInsets.all(10),
                    child: Text(
                      'Японская - Русская - Итальянская - Казахская - Грузинская - Европейская - Азиатская',
                      style: TextStyle(
                        fontSize: 12,
                        fontWeight: FontWeight.w700,
                        color: Colors.grey,
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ));
  }
}


