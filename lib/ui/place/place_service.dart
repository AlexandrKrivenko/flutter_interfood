import 'dart:convert';
import 'dart:async';
import 'package:http/http.dart' as http;

main() {
	getPlace(1);
}

class Place {
	final int id;
	final String name;
	final int price;

	Place.fromJson(Map jsonMap)
			: name = jsonMap['name'],
				price = jsonMap['price'],
				id = jsonMap['id'];

	String toString() => "${name}";
}

	Future<Stream<Place>>	getPlace(int id) async {
	var url = "https://edok.kz/api/rest/dishesv2?id_place=$id";
	var apiAuth = "mp@admplaces";
	var apiKey = "mp@7uf98HKHf";

	Map<String, String> headers = {
		"X-REST-USERNAME": apiAuth,
		"X-REST-PASSWORD": apiKey
	};

	final client = new http.Client();
	var req = new http.Request(
		'get',
		Uri.parse(url),
	);
	req.headers['X-REST-USERNAME'] = apiAuth;
	req.headers['X-REST-PASSWORD'] = apiKey;

	var streamRes = await client.send(req);

	return streamRes.stream
			.transform(Utf8Decoder())
			.transform(JsonDecoder())
			.expand((jsonBody) => (jsonBody as Map)['places'])
			.map((jsonMap) => new Place.fromJson(jsonMap));
}
