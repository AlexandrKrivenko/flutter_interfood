import 'package:flutter/material.dart';


class ItemCounter extends StatefulWidget {
  ItemCounter({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _ItemCounterState createState() => _ItemCounterState();
}
class _ItemCounterState extends State<ItemCounter> {
  int _counter = 0;

  void _incrementCounter() {
    setState(() {
      _counter++;
    });
  }

  void _dicrementCounter() {
    setState(() {
      _counter--;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.center,
      decoration: new BoxDecoration(
          border: new Border.all(color: Colors.grey),
          borderRadius: BorderRadius.circular(20)),
      height: 40,
      width: 120,
      child: Row(
        children: <Widget>[
          Spacer(),
          IconButton(
            onPressed: _dicrementCounter,
            highlightColor: Colors.black,
            splashColor: Colors.black,
            icon: Icon(Icons.remove),
            iconSize: 20,
          ),
          Spacer(),
          Text(
            '$_counter',
            style: TextStyle(
                color: Colors.deepOrange,
                fontSize: 15,
                fontWeight: FontWeight.w500),
            //style: Theme.of(context).textTheme.display1,
          ),
          Spacer(),
          IconButton(
              onPressed: _incrementCounter,
              highlightColor: Colors.black12,
              icon: Icon(Icons.add)),
          Spacer(),
        ],
      ),
    );
  }
}




class Trash extends StatelessWidget {
  const Trash({
    Key key,
    @required this.dishinfo
  }) : super(key: key);
  final Map dishinfo;

  @override
  Widget build(BuildContext context) {
    return 
        DishInfo(dishinfoitem: dishinfo)
      ;
  }
}

class DishInfo extends StatefulWidget {
  const DishInfo({Key key, @required this.dishinfoitem}) : super(key: key);
  final Map dishinfoitem;

  @override
  _DishInfoState createState() => _DishInfoState();
}

class _DishInfoState extends State<DishInfo> {
  @override
  Widget build(BuildContext context) {
    double c_width = MediaQuery.of(context).size.width * 0.8;
    return Scaffold(
        appBar: AppBar(
          title: Text(
            'name',
            style: TextStyle(
              color: Colors.black,
              fontSize: 16,
              fontWeight: FontWeight.w700,
            ),
            textAlign: TextAlign.center,
          ),
          actions: <Widget>[
            Container(
              child: IconButton(icon: Icon(Icons.search), onPressed: () {}),
              padding: EdgeInsets.all(10),
            ),
          ],
          centerTitle: true,
          iconTheme: IconThemeData(color: Colors.black),
          backgroundColor: Colors.white,
          brightness: Brightness.light,
          elevation: 0,
        ),
        body: ListView(
          children: <Widget>[
            Container(
                padding: EdgeInsets.fromLTRB(0, 0, 0, 20),
                color: Colors.white,
                child: Column(children: <Widget>[
                  Container(
                      child: Stack(children: <Widget>[
                    Center(
                      child: SizedBox(
                        width: 410,
                        height: 350,
                        child: Image.network(
                          'https://edok.kz/images/food_photo/${widget.dishinfoitem["photo"]}',
                          fit: BoxFit.fill,
                        ),
                      ),
                    ),
                  ])),
                  Container(
                      child: Wrap(alignment: WrapAlignment.end, children: <Widget>[
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Container(
                          padding: EdgeInsets.only(left: 8.0, top: 3),
                          width: c_width,
                          child: Text(
                            widget.dishinfoitem['name'],
                            textAlign: TextAlign.left,
                            style: TextStyle(
                              fontSize: 23,
                              color: Colors.black,
                              fontWeight: FontWeight.w700,
                            ),
                          ),
                        ),
                      ],
                    ),
                    Wrap(
                      alignment: WrapAlignment.end,
                      children: <Widget>[
                        Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Container(
                              padding: EdgeInsets.all(8.0),
                              width: c_width,
                              child: Text(
                                widget.dishinfoitem['description'],
                                style: TextStyle(
                                  fontSize: 14,
                                  color: Colors.black45,
                                  fontWeight: FontWeight.w500,
                                ),
                                textAlign: TextAlign.start,
                              ),
                            ),
                          ],
                        )
                      ],
                    ),
                    Container(
                      padding: EdgeInsets.only(left: 5),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: <Widget>[
                          Row(
                            children: <Widget>[
                              Text(
                                widget.dishinfoitem['price'],
                                textAlign: TextAlign.start,
                                style: TextStyle(
                                  fontSize: 22,
                                  color: Colors.black,
                                  fontWeight: FontWeight.w600,
                                ),
                              ),
                              Container(
                                padding: EdgeInsets.only(left: 5),
                                child: Text(
                                  '₸',
                                  style: TextStyle(
                                    fontSize: 22,
                                    color: Colors.black,
                                    fontWeight: FontWeight.w600,
                                  ),
                                ),
                              )
                            ],
                          ),
                          SizedBox(
                            width: 240,
                          ),
                          Text(
                            "1 шт.",
                            // textAlign: TextAlign.end,
                            style: TextStyle(
                              fontSize: 22,
                              color: Colors.black,
                              fontWeight: FontWeight.w600,
                            ),
                          )
                        ],
                      ),
                    ),
                  ])),
                  SizedBox(
                    height: 30,
                  ),
                  Container(
                    padding: EdgeInsets.only(left: 8.0),
                    child: Row(
                      children: <Widget>[
                        Text(
                          "Итого: ",
                          textAlign: TextAlign.start,
                          style: TextStyle(
                            fontSize: 22,
                            color: Colors.black,
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                        Text(
                          "7800",
                          style: TextStyle(
                            color: Colors.deepOrange,
                            fontSize: 22,
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                        SizedBox(
                          width: 60,
                        ),
                        Row(
                          //mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            Container(
                                padding: EdgeInsets.only(left: 80),
                                child: ItemCounter())
                          ],
                        ),
                      ],
                    ),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Container(
                        width: 350,
                        height: 60,
                        margin: EdgeInsets.only(top: 50),
                        decoration: new BoxDecoration(
                            border: new Border.all(color: Colors.deepOrange),
                            borderRadius: BorderRadius.circular(30)),
                        child: new MaterialButton(
                          shape: new RoundedRectangleBorder(
                              borderRadius: new BorderRadius.circular(30.0)),
                          color: Colors.white,
                          textColor: Colors.deepOrange,
                          child: new Text(
                            'Добавить 2 в корзину ',
                            style: TextStyle(fontSize: 18.0),
                          ),
                          onPressed: () => {},
                        ),
                      )
                    ],
                  )
                ])),
          ],
        ));
  }
}
