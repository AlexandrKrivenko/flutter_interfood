import 'package:flutter/material.dart';
import 'dart:async';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'trashbin.dart';

class ItemCounter extends StatefulWidget {
  ItemCounter({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _ItemCounterState createState() => _ItemCounterState();
}
class _ItemCounterState extends State<ItemCounter> {
  int _counter = 0;

  void _incrementCounter() {
    setState(() {
      _counter++;
    });
  }

  void _dicrementCounter() {
    setState(() {
      _counter--;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.center,
      decoration: new BoxDecoration(
          border: new Border.all(color: Colors.grey),
          borderRadius: BorderRadius.circular(20)),
      height: 40,
      width: 120,
      child: Row(
        children: <Widget>[
          Spacer(),
          IconButton(
            onPressed: _dicrementCounter,
            icon: Icon(Icons.remove),
            iconSize: 20,
          ),
          Spacer(),
          Text(
            '$_counter',
            style: TextStyle(
                color: Colors.deepOrange,
                fontSize: 15,
                fontWeight: FontWeight.w500),
            //style: Theme.of(context).textTheme.display1,
          ),
          Spacer(),
          IconButton(
              onPressed: _incrementCounter,
              highlightColor: Colors.black12,
              icon: Icon(Icons.add)),
          Spacer(),
        ],
      ),
    );
  }
}


class PlaceMenu extends StatefulWidget {
  const PlaceMenu({
    Key key,
    @required this.placeMenuList
  }) : super(key: key);
  final Map placeMenuList;

  @override
  _PlaceMenuState createState() => _PlaceMenuState();
}

class _PlaceMenuState extends State<PlaceMenu> {
  Map dishes;
  List DishesList;

  Future getData() async {
    http.Response response = await http.get(
        "https://edok.kz/api/rest/dishesv2?id_place=${widget.placeMenuList['id']}",
        headers: {
          "x-rest-username": "mp@admplaces",
          "x-rest-password": "mp@7uf98HKHf"
        });
    dishes = json.decode(response.body);
    setState(() {
      DishesList = dishes["dishes"];
    });
  }

  @override
  void initState() {
    super.initState();
    getData();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: ListView.builder(
          itemCount: DishesList == null ? 0 : DishesList.length,
          itemBuilder: (BuildContext context, int index) {
            return PlaceMenuItem(menu: DishesList[index],);
          }),
    );
  }
}



class PlaceMenuItem extends StatefulWidget {
  const PlaceMenuItem({
    Key key,
    @required this.menu
}) : super(key: key);
  final Map menu;
  @override
  _PlaceMenuItemState createState() => _PlaceMenuItemState();
}

class _PlaceMenuItemState extends State<PlaceMenuItem> {
  @override
  Widget build(BuildContext context) {
    return Card(
        child: GestureDetector(
          onTap: () {
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => Trash(dishinfo: widget.menu),
              ),
            );
          },
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Column(
               
                children: <Widget>[
                  Container(
                      padding: EdgeInsets.only(top: 7, left: 12),
                      width: 200,
                      child: Text(
                        '${widget.menu["name"]}',
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(
                            fontSize: 18, fontWeight: FontWeight.w600),
                      )),

                  Container(
                      padding:
                      EdgeInsets.only(top: 20, bottom: 20, right: 60),
                      child: ItemCounter()),

                  Container(
                    padding: EdgeInsets.only( bottom: 10, right: 125),
                    child: Row(
                        children: <Widget>[
                          Text(
                            '${widget.menu["price"]}',
                            style: TextStyle(
                                color: Colors.deepOrange,
                                fontSize: 18,
                                fontWeight: FontWeight.w600),
                          ),
                          Text('₸',
                            style: TextStyle(
                                color: Colors.deepOrange,
                                fontSize: 18,
                                fontWeight: FontWeight.w600),),
                        ]),
                  ),
                ],
              ),
              Container(
                
                padding: EdgeInsets.only(bottom: 7, top: 7,right: 10),
                child: SizedBox(
                  width: 120,
                  height: 120,
                  child: Image.network(
                      'https://edok.kz/images/food_photo/${widget.menu["photo"]}'
                  ),
                ),
              ),
            ],
          ),
        ));
  }
}
