import 'package:flutter/material.dart';

class CommentItem extends StatelessWidget {
	const CommentItem({
		Key key,
	}) : super(key: key);

	@override
	Widget build(BuildContext context) {
		return Card(
			child: Container(
				color: Colors.white,
				child: Column(
					children: <Widget>[
						Container(
							padding: EdgeInsets.all(10),
							child: Row(
								children: <Widget>[
									Text(
										'Джейми Оливер',
										style: TextStyle(color: Colors.grey),
									),
									Spacer(),
									Icon(
										Icons.star,
										color: Colors.deepOrange,
									),
									Icon(
										Icons.star,
										color: Colors.deepOrange,
									),
									Icon(
										Icons.star,
										color: Colors.deepOrange,
									),
									Icon(
										Icons.star_half,
										color: Colors.deepOrange,
									),
									Icon(
										Icons.star_border,
										color: Colors.deepOrange,
									)
								],
							),
						),
						Container(
							padding: EdgeInsets.all(10),
							child: Row(
								children: <Widget>[
									Text(
										'Вкусная еда, быстрая доставка',
										style: TextStyle(
											fontSize: 18,
											fontWeight: FontWeight.w700,
										),
									)
								],
							),
						),
						Container(
							padding: EdgeInsets.all(10),
							child: Text(
								"Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.",
								textAlign: TextAlign.justify,
							),
						),
						Container(
							padding: EdgeInsets.all(10),
							child: Row(
								children: <Widget>[
									Text(
										'26.12.19',
										style: TextStyle(color: Colors.grey),
									),
									Spacer(),
									Text(
										'Заказ №34234',
										style: TextStyle(color: Colors.grey),
									)
								],
							),
						),
					],
				),
			),
		);
	}
}
