import 'package:flutter/material.dart';

class AboutView extends StatefulWidget {
	const AboutView ({
		Key key,
		@required this.about
}) : super(key: key);
	final Map about;

	@override
	_AboutViewState createState() => _AboutViewState();
}

class _AboutViewState extends State<AboutView> {

	@override
	Widget build(BuildContext context) {
		return Container(
			color: Colors.white,
			child: Column(
				children: <Widget>[
					Container(
						padding: EdgeInsets.all(15),
						child: Row(
							children: <Widget>[
								Spacer(),
								Container(
									child: Container(
										child: Row(
											children: <Widget>[
												Container(
													child: Icon(
													Icons.access_time,
														color: Colors.white,
														size: 30,
													),
												),
												Container(
													padding: EdgeInsets.only(left: 5),
													width: 100,
												  child: Text('Доставка ${widget.about['average_delievery_time']} мин.',
												  	style: TextStyle(
												  			color: Colors.white,
												  			fontWeight: FontWeight.w600,
												  			fontSize: 16
												  	),),
												)
											],
										),
									),
									decoration: BoxDecoration(
										color: Colors.deepOrangeAccent,
										borderRadius: BorderRadius.circular(5),
									),
									padding: EdgeInsets.all(10),
									height: 55,
									width: 150,
								),
								Spacer(),
								Container(
									child: Container(
										width: 100,
											child: Row(
												children: <Widget>[
													Container(
														child: Icon(
															Icons.title,
															color: Colors.white,
															size: 30,
														),
													),
													Container(
														padding: EdgeInsets.only(left: 5),
														width: 100,
													  child: Text('Мин. заказ от ${widget.about['order_min_price']} тг.',
													  	style: TextStyle(
													  			color: Colors.white,
													  			fontWeight: FontWeight.w600,
													  			fontSize: 16
													  	),),
													)
												],
											),
											),
									decoration: BoxDecoration(
										color: Colors.deepOrangeAccent,
										borderRadius: BorderRadius.circular(5),
									),
									padding: EdgeInsets.all(10),
									height: 55,
									width: 150,
								),
								Spacer(),
							],
						),
					),
					Container(
						child: Column(
							children: <Widget>[
								Row(
									crossAxisAlignment: CrossAxisAlignment.start,
									children: <Widget>[
										Container(
											padding: EdgeInsets.all(15),
											child: Text(
												'О заведении',
												style: TextStyle(
													fontSize: 20,

													fontWeight: FontWeight.w700,
												),
												textAlign: TextAlign.left,
											),
										),
									],
								),
								Container(
									padding: EdgeInsets.all(15),
									child: Text(
										widget.about['description'],
										style: TextStyle(
											fontFamily: 'SFProDisplay',
											fontSize: 14.5,
										),

										textAlign: TextAlign.left,
									),
								)
							],
						),
					),
					Container(
						child: Column(
							children: <Widget>[
								Row(
									crossAxisAlignment: CrossAxisAlignment.start,
									children: <Widget>[
										Container(
											padding: EdgeInsets.all(15),
											child: Text(
												'Адрес',
												style: TextStyle(
													fontSize: 20,
													fontWeight: FontWeight.w700,
												),
												textAlign: TextAlign.left,
											),
										),
									],
								),
								Row(
									crossAxisAlignment: CrossAxisAlignment.start,
									children: <Widget>[
										Container(
											padding: EdgeInsets.fromLTRB(15, 0, 0, 0),
											child: Text(widget.about['address'],
												style: TextStyle(
													fontSize: 14.5,
												),),
										),
									],
								),
							],
						),
					),
					Container(
						padding: EdgeInsets.fromLTRB(0, 0, 100, 40),
						child: Column(
							children: <Widget>[
								Row(
									crossAxisAlignment: CrossAxisAlignment.start,
									children: <Widget>[
										Container(
											padding: const EdgeInsets.fromLTRB(15, 10, 0, 0),
											child: Text(
												'Режим работы',
												style: TextStyle(
													fontSize: 18,
													fontWeight: FontWeight.w700,
												),
											),
										),
									],
								),
								Stack(
									children: <Widget>[
										Column(
												children: <Widget>[
													Container(
															padding:EdgeInsets.only(left: 15),
															child: WorkMOde(workmode: widget.about['workmode']))
												],)
									],
								)
							],
						),
					)
				],
			),
		);
	}
}



class WorkMOde extends StatefulWidget {
	const WorkMOde({
		Key key,
		@required this.workmode
}) : super(key: key);
	final List workmode;
  @override
  _WorkMOdeState createState() => _WorkMOdeState();
}

class _WorkMOdeState extends State<WorkMOde> {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(right: 80),
	    child: Stack(
		    children: <Widget>[
		    	Container(
				    padding: EdgeInsets.only(top: 20),
		    	  child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
		    	    children: <Widget>[
		    	      Container(
					    child: Text('Понедельник',
					    style: TextStyle(
							    color: Colors.grey[600],
						    fontWeight: FontWeight.w500,
						    fontSize: 14.5
					    ),)
			        ),
             
				      Row(
					      children: <Widget>[
						      Text(widget.workmode[0]['work_from'],
							      style: TextStyle(
									      fontWeight: FontWeight.w500,
									      fontSize: 14.5
							      ),),
						      Text(' - ',
							      style: TextStyle(
									      fontSize: 14.5
							      ),),
						      Text(widget.workmode[0]['work_to'],
							      style: TextStyle(

									      fontWeight: FontWeight.w500,
									      fontSize: 14.5
							      ),),
					      ],
				      ),
		    	    ],
		    	  ),
		    	),


			    Container(
				    padding: EdgeInsets.only(top: 50, ),
			      child: Row(
				      mainAxisAlignment: MainAxisAlignment.spaceBetween,
			        children: <Widget>[
			          Container(
					    child: Text('Вторник',
						    style: TextStyle(
								    fontWeight: FontWeight.w500,
								    color: Colors.grey[600],
								    fontSize: 14.5
						    ),)
			          ),

				        Row(
					        children: <Widget>[
						        Text(widget.workmode[1]['work_from'],
							        style: TextStyle(
									        fontWeight: FontWeight.w500,

									        fontSize: 14.5
							        ),),
						        Text(' - ',
							        style: TextStyle(
									        fontSize: 14.5
							        ),),
						        Text(widget.workmode[1]['work_to'],
							        style: TextStyle(
									        fontWeight: FontWeight.w500,

									        fontSize: 14.5
							        ),),
					        ],
				        ),
			        ],
			      ),
			    ),

			    Container(
				    padding: EdgeInsets.only(top: 80, ),
				    child: Row(
					    mainAxisAlignment: MainAxisAlignment.spaceBetween,
					    children: <Widget>[
						    Container(
								    child: Text('Среда',
									    style: TextStyle(
										    color: Colors.grey[600],
											    fontWeight: FontWeight.w500,
											    fontSize: 14.5
									    ),)
						    ),
						    Row(
							    children: <Widget>[
								    Text(widget.workmode[2]['work_from'],
									    style: TextStyle(
											    fontWeight: FontWeight.w500,
											    fontSize: 14.5
									    ),),
								    Text(' - '),
								    Text(widget.workmode[2]['work_to'],
									    style: TextStyle(
											    fontWeight: FontWeight.w500,
											    fontSize: 14.5
									    ),),
							    ],
						    ),
					    ],
				    ),
			    ),

			    Container(
				    padding: EdgeInsets.only(top: 110, ),
				    child: Row(
					    mainAxisAlignment: MainAxisAlignment.spaceBetween,
					    children: <Widget>[
						    Container(
								    child: Text('Четверг',
									    style: TextStyle(
											    color: Colors.grey[600],
											    fontWeight: FontWeight.w500,
											    fontSize: 14.5
									    ),)
						    ),
						    Row(
							    children: <Widget>[
								    Text(widget.workmode[3]['work_from'],
									    style: TextStyle(
											    fontWeight: FontWeight.w500,
											    fontSize: 14.5
									    ),),
								    Text(' - '),
								    Text(widget.workmode[3]['work_to'],
									    style: TextStyle(
											    fontWeight: FontWeight.w500,
											    fontSize: 14.5
									    ),),
							    ],
						    ),

					    ],
				    ),
			    ),

			    Container(
				    padding: EdgeInsets.only(top: 140, ),
				    child: Row(
					    mainAxisAlignment: MainAxisAlignment.spaceBetween,
					    children: <Widget>[
						    Container(
								    child: Text('Пятница',
									    style: TextStyle(
											    color: Colors.grey[600],
											    fontWeight: FontWeight.w500,
											    fontSize: 14.5
									    ),)
						    ),
						    Row(
							    children: <Widget>[
								    Text(widget.workmode[4]['work_from'],
									    style: TextStyle(
											    fontWeight: FontWeight.w500,
											    fontSize: 14.5
									    ),),
								    Text(' - '),
								    Text(widget.workmode[4]['work_to'],
									    style: TextStyle(
											    fontWeight: FontWeight.w500,
											    fontSize: 14.5
									    ),),
							    ],
						    ),
					    ],
				    ),
			    ),

			    Container(
				    padding: EdgeInsets.only(top: 170, ),
				    child: Row(
					    mainAxisAlignment: MainAxisAlignment.spaceBetween,
					    children: <Widget>[
						    Container(
								    child: Text('Суббота',
									    style: TextStyle(
											    color: Colors.grey[600],
											    fontWeight: FontWeight.w500,
											    fontSize: 14.5
									    ),)
						    ),
						    Row(
							    children: <Widget>[
								    Text(widget.workmode[5]['work_from'],
									    style: TextStyle(
											    fontWeight: FontWeight.w500,
											    fontSize: 14.5
									    ),),
								    Text(' - '),
								    Text(widget.workmode[5]['work_to'],
									    style: TextStyle(
											    fontWeight: FontWeight.w500,
											    fontSize: 14.5
									    ),),
							    ],
						    ),
					    ],
				    ),
			    ),
			    Container(
				    padding: EdgeInsets.only(top: 200,),
				    child: Row(
					    mainAxisAlignment: MainAxisAlignment.spaceBetween,
					    children: <Widget>[
						    Container(
								    child: Text('Воскресение',
									    style: TextStyle(
											    fontWeight: FontWeight.w500,
											    color: Colors.grey[600],
											    fontSize: 14.5
									    ),)
						    ),
						    Row(
							    children: <Widget>[
								    Text(widget.workmode[6]['work_from'],
									    style: TextStyle(
											    fontWeight: FontWeight.w500,
											    fontSize: 14.5
									    ),),
								    Text(' - '),
								    Text(widget.workmode[6]['work_to'],
									    style: TextStyle(
											    fontWeight: FontWeight.w500,
											    fontSize: 14.5
									    ),),
							    ],
						    ),
					    ],
				    ),
			    ),


		    ],
	    ),
    );
  }
}
