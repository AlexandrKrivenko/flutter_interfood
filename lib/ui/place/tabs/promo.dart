import 'package:flutter/material.dart';
import 'dart:async';
import 'package:http/http.dart' as http;
import 'dart:convert';

class PromoItem extends StatefulWidget {
  const PromoItem({
    Key key,
    @required this.promo
  }) : super(key: key);
  final Map promo;

  @override
  _PromoItemState createState() => _PromoItemState();
}

class _PromoItemState extends State<PromoItem> {
  Map actions;
  List PromoList;

  Future getData() async {
    http.Response response = await http.get(
        "https://edok.kz/api/rest/actionsplace?id_place=${widget.promo['id']}",
        headers: {
          "x-rest-username": "mp@admplaces",
          "x-rest-password": "mp@7uf98HKHf"
        });
    actions = json.decode(response.body);
    setState(() {
      PromoList = actions["actions"];
    });
  }

  @override
  void initState() {
    super.initState();
    getData();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        height: 660,
        child: PromoList.isEmpty ? Center(
          child: Container(
            padding: EdgeInsets.only(bottom: 100),
          child: Text('На данный момент акций нет',
          style: TextStyle(
            fontSize: 19,
            fontWeight: FontWeight.w600
          ),))) 
        : ListView.builder(
            itemCount: PromoList == null ? 0 : PromoList.length,
            itemBuilder: (BuildContext context, int index) {
              return PromoItems(
                promoitem: PromoList[index],
              );
            }));
  }
}

class PromoItems extends StatefulWidget {
  const PromoItems({Key key, @required this.promoitem}) : super(key: key);
  final Map promoitem;

  @override
  _PromoItemsState createState() => _PromoItemsState();
}

class _PromoItemsState extends State<PromoItems> {
  @override
  Widget build(BuildContext context) {
    return Card(
      child: Stack(
        children: <Widget>[
          Container(
            padding: EdgeInsets.only(left: 20, right: 20),
            child: Text(widget.promoitem['title'],
            overflow: TextOverflow.ellipsis,
            style: TextStyle(
              fontSize: 20,
              fontWeight: FontWeight.w600
            ),),
          ),
          Container(
            padding: EdgeInsets.only(top: 80, left: 20, bottom: 10),
            child: Text(widget.promoitem['description'],
            style: TextStyle(
              fontSize: 14,
              color: Colors.grey[600]
            ),),
          )
        ],
      ),
    );
  }
}
