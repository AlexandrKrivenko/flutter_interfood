import 'package:edok/ui/cities/city.dart';
import 'package:edok/ui/main/home.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter/services.dart';
import 'dart:async';





class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {


  @override
  Widget build(BuildContext context) {
    //default value : width : 1080px , height:1920px , allowFontScaling:false
    ScreenUtil.instance = ScreenUtil.getInstance()..init(context);

//If the design is based on the size of the iPhone6 ​​(iPhone6 ​​750*1334)
    ScreenUtil.instance = ScreenUtil(width: 1080, height: 1920)..init(context);

//If you want to set the font size is scaled according to the system's "font size" assist option
    ScreenUtil.instance = ScreenUtil(width: 1080, height: 1920, allowFontScaling: true)..init(context);
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: LoginPageForm(),
       routes: <String, WidgetBuilder> {
        '/homepage': (BuildContext context) => CitiesName(),
        '/landingpage': (BuildContext context) => LoginPage(),
           }
    );
      }
}





class LoginPageForm extends StatefulWidget {
  @override
  _LoginPageFormState createState() => _LoginPageFormState();
}

class _LoginPageFormState extends State<LoginPageForm> {
 
  @override
  Widget build(BuildContext context) {
    //default value : width : 1080px , height:1920px , allowFontScaling:false
    ScreenUtil.instance = ScreenUtil.getInstance()..init(context);

//If the design is based on the size of the iPhone6 ​​(iPhone6 ​​750*1334)
    ScreenUtil.instance = ScreenUtil(width: 1080, height: 1920)..init(context);

//If you want to set the font size is scaled according to the system's "font size" assist option
    ScreenUtil.instance = ScreenUtil(width: 1080, height: 1920, allowFontScaling: true)..init(context);
    return Scaffold(
      backgroundColor: Colors.white,
      body: Column(
        children: <Widget>[
      Container(
        padding: EdgeInsets.only(top:20, left: 10, right: 10 ),
        child: Image(
        image: new AssetImage("assets/logo.jpg"),
        ),
      ),
          Flexible
            (
              child: Container(
                padding: EdgeInsets.only(top: 40),
                child: Text('Добро пожаловать!',
          textAlign: TextAlign.center,
          style: TextStyle(
                fontSize: 50,
            fontWeight: FontWeight.w700,
            color: Colors.orange
          ),),
              ),
            ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Container(
                padding: EdgeInsets.only(top: 20),
                child: Text('ВЫБЕРИТЕ ОПЦИЮ ЗАКАЗА',
                style: TextStyle(
                  color: Colors.orange,
                  fontSize: 17,
                  fontWeight: FontWeight.w500
                ),),
              )
            ],
          ),
    Row(
    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
    children: <Widget>[
       Container(
         width: 150,
          height: 200,
          margin: EdgeInsets.only(top: 50),
           decoration: new BoxDecoration(
               borderRadius: BorderRadius.circular(30)),
             child: new MaterialButton(
               shape: new RoundedRectangleBorder(
                 borderRadius: new BorderRadius.circular(10.0)),
                 color: Colors.orange,

                   child: new Text(
    'В зале ',
    style: TextStyle(fontSize: 18.0, color: Colors.white),
    ),
    onPressed: () => {},
    ),
    ),

      Container(
        width: 150,
        height: 200,
        margin: EdgeInsets.only(top: 50),
        decoration: new BoxDecoration(
            borderRadius: BorderRadius.circular(30)),
        child: new MaterialButton(
          shape: new RoundedRectangleBorder(
              borderRadius: new BorderRadius.circular(10.0)),
          color: Colors.orange,

          child: new Text(
            'На вынос ',
            style: TextStyle(fontSize: 18.0, color: Colors.white),
          ),
          onPressed: () => {
          Navigator.pushAndRemoveUntil(
          context,
          MaterialPageRoute(builder: (context) => Home()),
          (Route<dynamic> route) => false,
          )
          },
        ),
      )],
          ),
      ]));
  }
}


class Profile extends StatefulWidget {
  @override
  _ProfileState createState() => _ProfileState();
}

class _ProfileState extends State<Profile> {
  @override
  Widget build(BuildContext context) {
    return  MaterialApp(
      debugShowCheckedModeBanner: false,
      home: LoginPageForm(),
       routes: <String, WidgetBuilder> {
        '/landingpage': (BuildContext context) => LoginPage(),
          }
    );
  }
}

class ProfileView extends StatefulWidget {
  @override
  _ProfileViewState createState() => _ProfileViewState();
}

class _ProfileViewState extends State<ProfileView> {

  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Профиль',
          style: TextStyle(
              color: Colors.black, fontSize: 14, fontWeight: FontWeight.w700),
        ),
        centerTitle: true,
        backgroundColor: Colors.white,
        brightness: Brightness.light,
        elevation: 0.50,
      ),
      body: Container(
        child: ListView(
          children: <Widget>[
            Card(
              child: ListTile(
                leading: Icon(Icons.person),
                title: Text(
                  'Пользователь',
                  style: TextStyle(fontSize: 14, fontWeight: FontWeight.w700),
                ),
               
              ),
              color: Colors.white,
            ),
            Padding(
              padding: const EdgeInsets.all(20),
            ),
            Card(
              shape: BeveledRectangleBorder(),
              margin: EdgeInsets.all(0.4),
              child: ListTile(
                title: Text(
                  'История заказов',
                  style: TextStyle(fontSize: 14),
                ),
                onTap: () {
                  var router = new MaterialPageRoute(
                      builder: (BuildContext context) => new _HistoryView());
                  Navigator.of(context).push(router);
                },
                trailing: Icon(
                  Icons.arrow_right,
                  color: Colors.black,
                ),
              ),
            ),
            Card(
              shape: BeveledRectangleBorder(),
              margin: EdgeInsets.all(0.4),
              child: ListTile(
                title: Text(
                  'Выйти из профиля',
                  style: TextStyle(fontSize: 14),
                ),
                onTap: () {
                 
                },
              ),
            ),
          ],
        ),
      ),
      backgroundColor: Colors.grey.shade100,
    );
  }
}

class _HistoryView extends StatefulWidget {
  @override
  __HistoryViewState createState() => __HistoryViewState();
}

class __HistoryViewState extends State<_HistoryView> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'История заказов',
          style: TextStyle(
              color: Colors.black, fontSize: 14, fontWeight: FontWeight.w700),
        ),
        centerTitle: true,
        backgroundColor: Colors.white,
        brightness: Brightness.light,
        iconTheme: IconThemeData(color: Colors.black),
        elevation: 0,
      ),
      body: Container(
        child: Text('History container'),
        alignment: Alignment.center,
      ),
    );
  }
}
